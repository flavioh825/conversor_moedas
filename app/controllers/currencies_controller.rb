class CurrenciesController < ApplicationController
  before_action :set_variables  
  def new
    @symbols = load_symbols
  end
  
  def create
    @from = params[:from]
    @to = params[:to]
    @amount = params[:amount]
    res = @conn.post "#/convert/#{@amount}/#{@from}/#{@to}?app_id=#{@app_id}"
    byebug
  end

  private

  def set_variables
    @base_url = 'https://openexchangerates.org/api/'
    @app_id = 'bfcb324948644df1ae81d3ca5161e47b'
    @conn = Faraday.new(url: @base_url)
  end

  def load_symbols
    res = @conn.get "codes/dbc95df78e2018213cb653c0"
    hash = JSON.parse(res.body)
    hash
  end

end